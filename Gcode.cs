﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;

namespace Openscad2CNC
{
    class Gcode
    {
        const string _booleano = "(true|false)";
        const string formato_num = "F5";
        static char[] separadores = { ':', ',' };

        private double stepXY;
        private double stepZ;
        private double safety;
        private double feedSeek;
        private double feedXY;
        private double feedZ;
        private double feedPull;
        private double offset;
        private bool metric;

        public Gcode(bool metric, double broca, double stepXY, double stepZ, double safety, double feedSeek, double feedXY, 
            double feedZ, double feedPull)
        {
            this.stepXY = stepXY;
            this.stepZ = stepZ;
            this.safety = safety;
            this.feedSeek = feedSeek;
            this.feedXY = feedXY;
            this.feedZ = feedZ;
            this.feedPull = feedPull;
            this.offset = broca / 2;
            this.metric = metric;
        }

        public void header(ref StreamWriter salida)
        {
            if (metric)
                salida.WriteLine("G21");
            else
                salida.WriteLine("G20");
            salida.WriteLine("G90");
            salida.WriteLine("M03");
            salida.WriteLine();
        }

        public void footer(ref StreamWriter salida)
        {
            salida.WriteLine("G00 Z" + safety.ToString(formato_num) + " F" + feedPull.ToString());
            salida.WriteLine("G00 X0 Y0 F" + feedSeek.ToString());
            salida.WriteLine();
            salida.WriteLine("M5 ( Spindle off. )");
            salida.WriteLine("M9 ( Coolant off. )");
            salida.WriteLine("M2 ( Program end. )");
            salida.Close();
        }

        public void sphere(string argumentos, bool hollow, Coordenada traslado, ref StreamWriter salida)
        {
            double radio = 0, deltaZ = 0, i, futureZ, temp, interior, j;
            string[] args = argumentos.Replace(" ", "").Split(separadores);
            foreach (string s in args)
            {
                if (s.ToLower().Contains("r="))
                    radio = Convert.ToDouble(s.Substring(s.IndexOf('=') + 1));
                else if (s.ToLower().Contains("d="))
                    radio = Convert.ToDouble(s.Substring(s.IndexOf('=') + 1)) / 2;
                else
                    continue;
            }

            Coordenada centro = traslado.clonar();
            Coordenada start = traslado.clonar();
            salida.WriteLine("G00 Z" + safety.ToString(formato_num) + " F" + feedPull.ToString());
            if (hollow)
            {
                start.X -= radio;
                start.Z -= stepZ;
                salida.WriteLine("G00 X" + start.X.ToString(formato_num) + " Y" + start.Y.ToString(formato_num) + feedSeek.ToString());
                for (i = radio; i >= 0; i -= stepXY)
                {
                    arc(i - offset, 0, 360, start.Z, centro, ref salida);
                    Coordenada concentrico = start.clonar();
                    for (j = i - stepXY; j >= 0; j -= stepXY)
                    {
                        concentrico.X += stepXY;
                        arc(j - offset, 0, 360, concentrico.Z, centro, ref salida);
                    }
                    start.X += stepXY;
                    interior = Math.Pow(radio, 2) - Math.Pow(start.X - centro.X, 2) - Math.Pow(start.Y - centro.Y, 2);
                    futureZ = centro.Z - Math.Sqrt(interior);
                    deltaZ = Math.Abs(start.Z - futureZ);
                    temp = stepXY;
                    while (deltaZ > stepZ)
                    {
                        temp /= 2;
                        start.X -= temp;
                        i += temp;
                        interior = Math.Pow(radio, 2) - Math.Pow(start.X - centro.X, 2) - Math.Pow(start.Y - centro.Y, 2);
                        futureZ = centro.Z - Math.Sqrt(interior);
                        deltaZ = Math.Abs(start.Z - futureZ);
                    }
                    start.Z = futureZ;
                }
            }
            else
            {
                bool lastPass = false;
                double delta = stepXY;
                start.Z = centro.Z + radio;
                salida.WriteLine("G00 X" + start.X.ToString(formato_num) + " Y" + start.Y.ToString(formato_num) + feedSeek.ToString());
                for (i = 0; i <= radio; i += delta)
                {
                    if (lastPass)
                        break;
                    arc(i + offset, 0, 360, start.Z, centro, ref salida);
                    Coordenada concentrico = start.clonar();
                    for (j = i + stepXY; j <= radio; j += stepXY)
                    {
                        concentrico.X -= stepXY;
                        arc(j + offset, 0, 360, start.Z, centro, ref salida);
                    }
                    do
                    {
                        interior = Math.Pow(radio, 2) - Math.Pow(start.X - delta - centro.X, 2) - Math.Pow(start.Y - centro.Y, 2);
                        futureZ = Math.Sqrt(interior) + centro.Z;
                        deltaZ = Math.Abs(start.Z - futureZ);
                        temp = stepXY;
                        if (deltaZ > stepZ)
                            delta = delta / 2;
                        salida.WriteLine("(Delta: " + delta + ")");
                        salida.WriteLine("(X= " + start.X + ")");
                    } while (deltaZ > stepZ);
                    if (i + delta > radio)
                    {
                        i = radio - delta;
                        start.X = delta;
                        interior = Math.Pow(radio, 2) - Math.Pow(start.X - delta - centro.X, 2) - Math.Pow(start.Y - centro.Y, 2);
                        futureZ = Math.Sqrt(interior) + centro.Z;
                        lastPass = true;
                    }
                    else
                        start.X -= delta;
                    start.Z = futureZ;
                }
            }
        }

        public void cube(string argumentos, bool hollow, Coordenada traslado, ref StreamWriter salida)
        {
            if (argumentos.Split(separadores).Length == 1)// ej. cube(10)
                cube(argumentos + "," + argumentos + "," + argumentos, hollow, traslado, ref salida); //  ej. cube(10) ==> cube([10,10,10])
            else
            {
                string[] args = argumentos.Split(separadores);
                Coordenada recorrido = new Coordenada(args[0], args[1], args[2]);
                Coordenada start = traslado.clonar();
                double i;
                int j;
                start.X += offset;
                start.Y += offset;
                start.Z += recorrido.Z;

                if (!hollow)
                {
                    recorrido.X += (offset * 2);
                    recorrido.Y += (offset * 2);
                    start.X -= (offset * 2);
                    start.Y -= (offset * 2);
                    edgeCube(recorrido, start, traslado, ref salida);
                }
                else if ((recorrido.X < (4 * offset)) || (recorrido.Y < (4 * offset)))
                {
                    recorrido.X -= (offset * 2);
                    recorrido.Y -= (offset * 2);
                    edgeCube(recorrido, start, traslado, ref salida);
                }
                else
                {
                    recorrido.X -= (offset * 2);
                    recorrido.Y -= (offset * 2);

                    salida.WriteLine("G00 Z" + safety.ToString(formato_num) + " F" + feedPull.ToString());
                    double factor = (recorrido.X + 1) / (recorrido.Y + 1);
                    int centro = Convert.ToInt32((recorrido.Y + stepXY) / (2 * stepXY));
                    if (factor == 1)
                        salida.WriteLine("G00 X" + (start.X + (recorrido.X / 2)).ToString(formato_num) + " Y" + (start.Y + (recorrido.Y / 2)).ToString(formato_num) + " F" + feedSeek.ToString());
                    else if (factor > 1)
                        salida.WriteLine("G00 X" + (start.X + ((recorrido.X / 2) / factor)).ToString(formato_num) + " Y" + (start.Y + (recorrido.Y / 2)).ToString(formato_num) + " F" + feedSeek.ToString());
                    else
                    {
                        salida.WriteLine("G00 X" + (start.X + (recorrido.X / 2)).ToString(formato_num) + " Y" + (start.Y + ((recorrido.Y / 2) * factor)).ToString(formato_num) + " F" + feedSeek.ToString());
                        centro = Convert.ToInt32((recorrido.X + stepXY) / (2 * stepXY));
                    }
                    salida.WriteLine("G00 Z" + start.Z.ToString(formato_num) + " F" + feedZ.ToString());

                    if (stepZ > recorrido.Z)
                        i = recorrido.Z;
                    else
                        i = stepZ;

                    for (; i <= recorrido.Z; i = i + stepZ) //La variable i controla el movimiento en Z
                    {
                        salida.WriteLine("G01 Z" + (0 - i + start.Z).ToString(formato_num) + " F" + feedZ.ToString());
                        for (j = centro; j >= 0; j--)
                        {
                            salida.WriteLine("G01 Y" + (recorrido.Y + start.Y - (j * stepXY)).ToString(formato_num) + " F" + feedXY.ToString());
                            salida.WriteLine("G01 X" + (recorrido.X + start.X - (j * stepXY)).ToString(formato_num) + " F" + feedXY.ToString());
                            salida.WriteLine("G01 Y" + (start.Y + (j * stepXY)).ToString(formato_num) + " F" + feedXY.ToString());
                            salida.WriteLine("G01 X" + (start.X + (j * stepXY)).ToString(formato_num) + " F" + feedXY.ToString());
                        }
                        salida.WriteLine("G01 Y" + (start.Y + recorrido.Y).ToString(formato_num) + " F" + feedXY.ToString());
                        salida.WriteLine("G01 X" + (start.X + recorrido.X).ToString(formato_num) + " F" + feedXY.ToString());
                        salida.WriteLine("G00 Z" + safety.ToString(formato_num) + " F" + feedPull.ToString());
                        if (factor == 1)
                            salida.WriteLine("G00 X" + (start.X + (recorrido.X / 2)).ToString(formato_num) + " Y" + (start.Y + (recorrido.Y / 2)).ToString(formato_num) + " F" + feedSeek.ToString());
                        else if (factor > 1)
                            salida.WriteLine("G00 X" + (start.X + ((recorrido.X / 2) / factor)).ToString(formato_num) + " Y" + (start.Y + (recorrido.Y / 2)).ToString(formato_num) + " F" + feedSeek.ToString());
                        else
                            salida.WriteLine("G00 X" + (start.X + (recorrido.X / 2)).ToString(formato_num) + " Y" + (start.Y + (recorrido.Y * factor / 2)).ToString(formato_num) + " F" + feedSeek.ToString());
                        if ((i < recorrido.Z) && ((i + stepZ) > recorrido.Z))
                            i = recorrido.Z - stepZ;
                    }
                    salida.WriteLine("G00 Z" + safety.ToString(formato_num) + " F" + feedPull.ToString());
                }
            }
        }

        private void edgeCube(Coordenada recorrido, Coordenada start, Coordenada traslado, ref StreamWriter salida)
        {
            double i;
            salida.WriteLine("G00 Z" + safety.ToString(formato_num) + " F" + feedPull.ToString());
            salida.WriteLine("G00 X" + start.X.ToString(formato_num) + " Y" + start.Y.ToString(formato_num) + " F" + feedSeek.ToString());
            salida.WriteLine("G00 Z" + start.Z.ToString(formato_num) + " F" + feedZ.ToString());
            for (i = stepZ; i <= recorrido.Z; i = i + stepZ) //La variable i controla el movimiento en Z
            {
                if ((i + stepZ) > recorrido.Z)
                    i = recorrido.Z;
                salida.WriteLine("G01 Z" + (start.Z - i).ToString(formato_num) + " F" + feedZ.ToString());
                salida.WriteLine("G01 Y" + (recorrido.Y + start.Y).ToString(formato_num) + " F" + feedXY.ToString());
                salida.WriteLine("G01 X" + (recorrido.X + start.X).ToString(formato_num) + " F" + feedXY.ToString());
                salida.WriteLine("G01 Y" + start.Y.ToString(formato_num) + " F" + feedXY.ToString());
                salida.WriteLine("G01 X" + start.X.ToString(formato_num) + " F" + feedXY.ToString());
                salida.WriteLine("G00 Z" + safety.ToString(formato_num) + " F" + feedPull.ToString());
            }
        }

        public void cylinder(string argumentos, bool hollow, Coordenada traslado, ref StreamWriter salida)
        {
            string[] args = argumentos.Replace(" ", "").Split(separadores);
            double[] parametros = { 0, 0, 0 };
            double cteStep = 0, r3 = 0, i;
            bool center = false;
            #region Obtener parametros
            for (int j = 0; j < args.Length; j++)
            {
                try
                {
                    if (j < 3)
                        parametros[j] = Convert.ToDouble(args[j]);
                    else
                        center = Convert.ToBoolean(System.Text.RegularExpressions.Regex.Match(args[j], _booleano).ToString());
                }
                catch (FormatException ex)
                {
                    if (args[j].ToLower().StartsWith("h="))
                        parametros[0] = Convert.ToDouble(args[j].Substring(args[j].IndexOf('=') + 1));
                    else if (args[j].ToLower().StartsWith("r1="))
                        parametros[1] = Convert.ToDouble(args[j].Substring(args[j].IndexOf('=') + 1));
                    else if (args[j].ToLower().StartsWith("r2="))
                        parametros[2] = Convert.ToDouble(args[j].Substring(args[j].IndexOf('=') + 1));
                    else if (args[j].ToLower().StartsWith("r="))
                    {
                        parametros[1] = Convert.ToDouble(args[j].Substring(args[j].IndexOf('=') + 1));
                        parametros[2] = parametros[1];
                    }
                    else if (args[j].ToLower().StartsWith("d1="))
                        parametros[1] = Convert.ToDouble(args[j].Substring(args[j].IndexOf('=') + 1)) / 2;
                    else if (args[j].ToLower().StartsWith("d2="))
                        parametros[2] = Convert.ToDouble(args[j].Substring(args[j].IndexOf('=') + 1)) / 2;
                    else if (args[j].ToLower().StartsWith("d="))
                    {
                        parametros[1] = Convert.ToDouble(args[j].Substring(args[j].IndexOf('=') + 1)) / 2;
                        parametros[2] = parametros[1];
                    }
                    else if (args[j].ToLower().StartsWith("center="))
                        center = Convert.ToBoolean(args[j].Replace("center=", "").Trim());
                }
            }
            #endregion
            Coordenada centro = traslado.clonar();
            Coordenada start = traslado.clonar();

            cteStep = (Math.Abs((parametros[2] - parametros[1]))) / ((parametros[0] - 1) / stepZ);

            if (hollow)
            {
                parametros[2] -= offset;
                parametros[1] -= offset;
            }
            else
            {
                parametros[2] += offset;
                parametros[1] += offset;
            }

            start.X -= parametros[2];
            if (center)
                start.Z += (parametros[0] / 2);
            else
                start.Z += (parametros[0]);

            salida.WriteLine("G00 Z" + safety.ToString(formato_num) + " F" + feedPull.ToString());
            if (stepZ > parametros[0])
                i = parametros[0];
            else
                i = stepZ;
            for (; i <= parametros[0]; i = i + stepZ)
            {
                double inicio = start.X;
                double radio = parametros[2];
                arc(radio, 0, 360, (0 - i + start.Z), start.Z, centro, ref salida);
                while (radio < (parametros[1]))
                {
                    radio += stepXY;
                    inicio -= stepXY;

                    if (radio > (parametros[1]))
                    {
                        inicio = inicio + (radio - (parametros[1]));
                        radio = parametros[1];
                    }
                    arc(radio, 0, 360, (0 - i + start.Z), start.Z, centro, ref salida);
                    if (radio == parametros[1])
                        break;
                }
                if (hollow)
                {
                    for (r3 = parametros[2] - stepXY; r3 > 0; r3 -= stepXY)
                    {
                        salida.WriteLine("(Loop)");
                        arc(r3, 0, 360, (0 - i + start.Z), start.Z, centro, ref salida);
                        if (r3 / offset < 1.001)
                            break;
                        if ((r3 - stepXY < offset) && (r3 > stepXY))
                            r3 = offset + stepXY;
                    }
                }
                if (!hollow)
                {
                    parametros[2] += cteStep;
                    start.X -= cteStep;
                }
                else
                {
                    parametros[2] -= cteStep;
                    start.X += cteStep;
                }
                if ((i < parametros[0]) && ((i + stepZ) > parametros[0]))
                    i = parametros[0] - stepZ;
            }
            salida.WriteLine("G00 Z" + safety.ToString(formato_num) + " F" + feedPull.ToString());
        }

        private void arc(double radius, double theta1, double theta2, double Z, 
            Coordenada centro, ref StreamWriter salida)
        {
            arc(radius, theta1, theta2, Z, Z, centro, ref salida);
        }

        private void arc(double radius, double theta1, double theta2, double Z, double startZ, 
            Coordenada centro, ref StreamWriter salida)
        {
            double Xs = centro.X + (radius * Math.Cos(theta1 * Math.PI / 180));
            double Ys = centro.Y + (radius * Math.Sin(theta1 * Math.PI / 180));
            double Xe = centro.X + (radius * Math.Cos(theta2 * Math.PI / 180));
            double Ye = centro.Y + (radius * Math.Sin(theta2 * Math.PI / 180));
            double I = (centro.X - (radius * Math.Cos(theta1 * Math.PI / 180))) - centro.X;
            double J = (centro.Y - (radius * Math.Sin(theta1 * Math.PI / 180))) - centro.Y;

            salida.WriteLine("G00 X" + Xs.ToString(formato_num) + " Y" + Ys.ToString(formato_num) + " F" + feedSeek.ToString());
            salida.WriteLine("G01 Z" + Z.ToString(formato_num) + " F" + feedZ.ToString());
            salida.WriteLine("G03 X" + Xe.ToString(formato_num) + " Y" + Ye.ToString(formato_num) + " I" + I.ToString(formato_num) + " J" + J.ToString(formato_num) + " F" + feedXY.ToString());
        }
    }
}
