﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Openscad2CNC
{
    class Constante
    {
        public string indice;
        public string valor;

        public Constante(string indice, string valor)
        {
            this.indice = indice;
            this.valor = valor;
        }
    }
}
