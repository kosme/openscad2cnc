OpenSCAD2CNC
============

C# CLI tool for generating toolpaths for CNC machines from OpenSCAD files. It is in constant development and currently supports this commands:

 - Cube
 - Cylinder
 - Sphere
 - Translate
 - Union
 - Difference
 - For cycles
 - Trigonometric functions

Usage
-----
Openscad2CNC [input_file] [output_file] [metric] [bit_diameter] [XY_step] [pass_depth] [safe_height][seek_feed] [XY_cut_feed] [plunge_rate] [pull_speed]

**[input_file]** OpenSCAD source file.

**[output_file]** File where the generated G-code will be stored.

**[metric]** Boolean value. *true* for file in mm, *false* for file in inches.

**[bit_diameter]** Diameter of routing bit.

**[XY_step]** Units between parallel cutting passes.

**[pass_depth]** Units the bit travels on Z axis on each pass.

**[safe_height]** Safe height between working surface and bit end.

**[seek_feed]** Speed in inches per minute for G00 commands.

**[XY_cut_feed]** Speed in inches per minute for G01/02 commands.

**[plunge_rate]** Speed in inches per minute for G01 commands on the Z axis.

**[pull_speed]** Speed in inches per minute for G00 commands for the Z axis.

Missing commands and features
-------

 - Rotate
 - Importing DXF files.
 - Minkowski
 - Hull
 - Polygon
 - Translating the object on the Z axis so the surface is at Z 0
 - GUI
 - Port to another language to support Linux users.
 - Nested loops.

 Changelog
-------

0.2 (01/30/16)
 - Implement code to solve arithmetic operations without depending on DataTables.Compute().
0.1.3 (01/29/16)
 - Cut cubes with spiral movement from inside to outside.
0.1.2 (11/02/15)
 - Implement trigonometric functions.
0.1.1 (10/15/15)
 - Create Arc function.
 - Replace circle code in Cylinders with Arc function.
0.1 (10/14/15) Initial release