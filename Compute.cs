﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Text.RegularExpressions;

namespace Openscad2CNC
{
    class Compute
    {
        static Regex number = new Regex("[0-9]*\\.?[0-9]+");
        static Regex numberComponent = new Regex("([0-9]|\\.)");

        public string solveString(string input)
        {
            Stack<string> pila = new Stack<string>();
            int prevPrecedence = 0;
            int precedence;
            string segment = "";
            while(input.Length>0)
            {
                string num = "";
                if(numberComponent.IsMatch(input[0].ToString()))
                {
                    num = number.Match(input).Value;
                    input = input.Substring(num.Length);
                }

                if (num.Length > 0)
                {
                    pila.Push(num);
                    continue;
                }
                else
                {
                    precedence = Precedence(input[0]);
                    if (((precedence > prevPrecedence) && (input[0] != ')')) || (input[0] == '('))
                    {
                        prevPrecedence = precedence;
                        pila.Push(input[0].ToString());
                    }
                    else
                    {
                        prevPrecedence = 0;
                        segment = "";
                        while ((pila.Count > 0) && (pila.Peek() != "("))
                            segment = segment + pila.Pop();
                        if (pila.Count > 0)
                            pila.Pop();
                        pila.Push(solveSegment(segment));
                        if (input[0] != ')')
                            pila.Push(input[0].ToString());
                    }
                    input = input.Substring(1);
                }
            }

            segment = "";
            prevPrecedence = 0;
            while (pila.Count > 0)
            {
                if (!number.IsMatch(pila.Peek()))
                {
                    if (prevPrecedence == 0)
                        prevPrecedence++;
                    else
                    {
                        segment = solveSegment(segment);
                        prevPrecedence = 0;
                    }
                }
                segment = segment + pila.Pop();
            }

            return solveSegment(segment);
        }

        static string solveSegment(string input)
        {
            string op1 = "", op2 = "";
            foreach (Match m in number.Matches(input))
            {
                if (op2 == "")
                {
                    op2 = m.Value;
                    input = input.Substring(input.IndexOf(op2) + op2.Length);
                    if (input.Length == 0)
                        return op2;
                }
                else if (op1 == "")
                {
                    op1 = m.Value;
                    input = input.Replace(op1, "");
                    if (input.Length == 0)
                    {
                        input = "-";
                        op1 = op1.Substring(1);
                    }
                }
            }

            if ((op1.Length == 0)&&(input.Length==0))
                return op2;

            switch (input.Trim())
            {
                case "+":
                    return "" + (Convert.ToDouble(op1) + Convert.ToDouble(op2));
                case "-":
                    if (op1 != "")
                        return "" + (Convert.ToDouble(op1) - Convert.ToDouble(op2));
                    else
                        return "-" + op2;
                case "/":
                    return "" + (Convert.ToDouble(op1) / Convert.ToDouble(op2));
                case "*":
                    return "" + (Convert.ToDouble(op1) * Convert.ToDouble(op2));
                case "%":
                    return "" + (Convert.ToDouble(op1) % Convert.ToDouble(op2));
                default:
                    return "0";
            }
        }

        static int Precedence(char element)
        {
            int res = 0;
            switch (element)
            {
                case '*':
                case '/':
                case '%':
                    res = 2;
                    break;
                case '+':
                case '-':
                    res = 1;
                    break;
            }
            return res;
        }
    }
}
