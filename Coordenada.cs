﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Text.RegularExpressions;

namespace Openscad2CNC
{
    class Coordenada
    {
        private static string numero = "-?[0-9]*\\.?[0-9]+";
        public double X;
        public double Y;
        public double Z;

        public Coordenada()
        {
        }

        ~Coordenada()
        {
        }

        public Coordenada(double x, double y, double z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public Coordenada(string s)
        {
            Regex coord = new Regex("^(?i)" + numero + ",\\s?" + numero + ",\\s?" + numero + "$");
            Regex coordSimple = new Regex("^(?i)" + numero + "$");
            Coordenada temp;
            if (coord.IsMatch(s.Trim()))
            {
                temp = new Coordenada(s.Substring(0, s.IndexOf(',')),
                    s.Substring(s.IndexOf(',') + 1, s.LastIndexOf(',') - s.IndexOf(',') - 1),
                    s.Substring(s.LastIndexOf(',') + 1));
            }
            else if (coordSimple.IsMatch(s.Trim()))
            {
                temp = new Coordenada(s, s, s);
            }
            else
            {
                temp = new Coordenada(null, null, null);
            }
            this.X = temp.X;
            this.Y = temp.Y;
            this.Z = temp.Z;
        }

        public Coordenada(string x, string y, string z)
        {
            this.X = Convert.ToDouble(x);
            this.Y = Convert.ToDouble(y);
            this.Z = Convert.ToDouble(z);
        }

        public Coordenada(Coordenada coord)
        {
            this.X = coord.X;
            this.Y = coord.Y;
            this.Z = coord.Z;
        }

        public Coordenada clonar()
        {
            return new Coordenada(this.X, this.Y, this.Z);
        }

        public static Coordenada operator +(Coordenada c1, Coordenada c2)
        {
            return new Coordenada(c1.X + c2.X, c1.Y + c2.Y, c1.Z + c2.Z);
        }

        public static Coordenada operator -(Coordenada c1, Coordenada c2)
        {
            return new Coordenada(c1.X - c2.X, c1.Y - c2.Y, c1.Z - c2.Z);
        }

        public static Coordenada operator *(Coordenada c1, double i)
        {
            return new Coordenada(c1.X * i, c1.Y * i, c1.Z * i);
        }

        public static Coordenada operator *(double i, Coordenada c1)
        {
            return c1 * i;
        }
    }
}
