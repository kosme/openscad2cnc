﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using System.IO;

namespace Openscad2CNC
{
    class Program
    {
        #region Declaracion de variables y objetos
        static bool inside = false;

        const string formato_num = "F5";

        const string _numero = "-?[0-9]*\\.?[0-9]+";
        const string _entero = "[0-9]";
        const string _letra = "[a-zA-Z]";
        const string _identificador = _letra + "(\\w)*";
        const string _asignacion = _identificador + "\\s?=\\s?";

        static LinkedList<string> operaciones = new LinkedList<string>();
        static LinkedList<Constante> constantes = new LinkedList<Constante>();
        static LinkedList<string> parametros = new LinkedList<string>();
        static LinkedList<string> iteradores = new LinkedList<string>();

        static LinkedList<string> pendientes = new LinkedList<string>();

        static Stack<string> modificadores = new Stack<string>();
        static Stack<Coordenada> translate = new Stack<Coordenada>();
        static Stack<bool> llaves = new Stack<bool>();

        static Regex defLlave = new Regex("^(\\{|\\})$");
        static Regex defConstante = new Regex("^(?i)[a-z]([a-z0-9]_?)*\\s?\\=");
        static Regex defTransforma = new Regex("^(?i)(rotate|translate)");
        static Regex defCombina = new Regex("^(?i)(union|difference|intersection)\\(\\)\\{?$");
        static Regex defOperacion = new Regex(_numero + "([\\+|\\-|\\*|\\/]" + _numero + ")+");

        static Regex operacionesTrigonometricas = new Regex("(?i)(cos|sin|tan|acos|asin|atan|atan)");
        static Regex operacionesMath = new Regex("(?i)(abs|ceil|concat|cross|exp|floor|ln|len|let|log|lookup|max|min|norm|pow|rands|round|sign|sqrt)");

        static char[] separadores = { ':', ',' };

        static bool alerta = false;

        static bool difference = false;
        static bool union = false;

        static StreamReader lector;

        static StreamWriter salida;

        static Coordenada traslado = new Coordenada(0, 0, 0);

        static Compute solve = new Compute();

        static Gcode gcode; 

        static double altura_maxima = 0;

        #endregion

        static void Main(string[] args)
        {
            #region get params
            if (args.Length == 11)
            {

                try
                {
                    lector = new StreamReader(args[0]);
                    salida = new StreamWriter(args[1], false); //no append
                    gcode = new Gcode(Convert.ToBoolean(args[2]),//metric
                        Convert.ToDouble(args[3]),//broca
                        Convert.ToDouble(args[4]),//stepXY
                        Convert.ToDouble(args[5]), //stepZ
                        Convert.ToDouble(args[6]), //safety
                        Convert.ToDouble(args[7]), //feedSeek
                        Convert.ToDouble(args[8]), //feedXY
                        Convert.ToDouble(args[9]), //feedZ
                        Convert.ToDouble(args[10]) //feedPull
                        );
                }
                catch (FormatException ex)
                {
                    Console.WriteLine("Wrong parameter format");
                    Console.Write("Usage:\nOpenscad2CNC [input_file] [output_file] [metric] [bit_diameter] [XY_step] [pass_depth] [safe_height] ");
                    Console.WriteLine("[seek_feed] [XY_cut_feed] [plunge_rate] [pull_speed]");
                    return;
                }
            }
            else
            {
                Console.WriteLine("Wrong parameters");
                Console.Write("Usage:\nOpenscad2CNC [input_file] [output_file] [metric] [bit_diameter] [XY_step] [pass_depth] [safe_height] ");
                Console.WriteLine("[seek_feed] [XY_cut_feed] [plunge_rate] [pull_speed]");
                return;
            }
            #endregion

            translate.Push(new Coordenada(0, 0, 0));
            llaves.Push(true);

            leerLineas();
            codigoIntermedio();
            //buscarAlturaMayor();
            int num_linea = 1;
            gcode.header(ref salida);
            foreach (string s in operaciones)
            {
                Console.WriteLine(s);
                analizarSintaxis(s);
                num_linea++;
            }
            gcode.footer(ref salida);
        }

        private static string reemplazarConstantes(string entrada)
        {
            Regex defIndiceConstante = new Regex("(?i)[a-z]([a-z0-9]_?)*");
            Regex defConstanteArray = new Regex(defIndiceConstante.ToString() + "\\[.+?\\]");
            string salida = entrada;
            while (defConstanteArray.IsMatch(salida))
            {
                string id = defIndiceConstante.Match(defConstanteArray.Match(salida).ToString()).ToString();
                foreach (Constante constante in constantes)
                {
                    if (id.Equals(constante.indice))
                    {
                        string[] args = quitarCorchete(constante.valor).Split(separadores);
                        string coincidencias = defConstanteArray.Match(salida).ToString();
                        int index = Convert.ToInt16(quitarCorchete(coincidencias.Replace(id, "")));
                        salida = salida.Replace(defConstanteArray.Match(salida).ToString(), args[index]);
                    }
                }
            }
            foreach(Match m in defIndiceConstante.Matches(salida))
            {
                string indice = m.ToString();
                foreach (Constante constante in constantes)
                {
                    if (indice.Equals(constante.indice))
                    {
                        salida = salida.Replace(constante.indice, constante.valor);
                        break;
                    }
                }
            }
            return salida;
        }

        private static void expandirIteradores(LinkedListNode<string> entrada, string variable, string argumentos)
        {
            Regex defFor2Args = new Regex("^[^:]*:[^:]*$");
            //Regex defFor2Args = new Regex("^" + _numero + ":" + _numero + "$");
            Regex defFor3Args = new Regex("^[^:]*:[^:]*:[^:]*$");
            //Regex defFor3Args = new Regex("^" + _numero + ":" + _numero + ":" + _numero + "$");
            LinkedListNode<string> nodo = entrada;
            int llaves = 0;
            bool salida = false;
            bool quitado = false;
            do
            {
                nodo = nodo.Next;
                salida = true;
                if (nodo.Value == "{")
                {
                    llaves++;
                    if (nodo.Previous.Value.ToLower().Contains("for"))
                        quitado = true;
                    else
                        iteradores.AddLast(nodo.Value);
                }
                else if (nodo.Value == "}")
                {
                    llaves--;
                    if (llaves == 0 && quitado)
                    { }
                    else
                        iteradores.AddLast(nodo.Value);
                }
                else
                {
                    iteradores.AddLast(nodo.Value);
                    if (defTransforma.IsMatch(nodo.Value) || defCombina.IsMatch(nodo.Value))
                        salida = false;
                }
            }
            while ((llaves > 0) || (salida == false));
            if (defFor3Args.IsMatch(argumentos))
            {
                Console.WriteLine("Expanding \"for cycle\"");
                expandFor3Args(entrada, variable, argumentos);
            }
            else if (defFor2Args.IsMatch(argumentos))
            {
                Console.WriteLine("Expanding \"for cycle\"");
                expandFor2Args(entrada, variable, argumentos);
            }
            
            nodo = nodo.Next;
            while(!nodo.Previous.Value.ToLower().Contains("for"))
            {
                operaciones.Remove(nodo.Previous);
            }
            operaciones.Remove(nodo.Previous);
        }

        static void analizarSintaxis(string linea)
        {
            #region Expresiones Regulares
            Regex defVacio = new Regex("^\\s*$");
            Regex defSolidos = new Regex("^(?i)(cube|cylinder|sphere|polyhedron)");
            Regex defComentarioLinea = new Regex("^\\/\\/.+");
            Regex defComentarioInicio = new Regex("^\\/\\*");
            Regex defComentarioFin = new Regex("\\*\\/");
            #endregion

                #region solidos
                if (defSolidos.IsMatch(linea))
                {
                    string solido = defSolidos.Match(linea).Value;
                    string argumentos = quitarCorchete(linea.Substring(linea.IndexOf('(') + 1, linea.LastIndexOf(')') - linea.IndexOf('(') - 1));
                    argumentos = quitarCorchete(reemplazarConstantes(argumentos));
                    argumentos = resolverOperaciones(argumentos);
                    if (difference)
                        pendientes.AddLast(linea);
                    else
                    {
                        switch (solido)
                        {
                            case "cube":
                                gcode.cube(argumentos, inside, traslado, ref salida);
                                break;
                            case "cylinder":
                                gcode.cylinder(argumentos, inside, traslado, ref salida);
                                break;
                            case "sphere":
                                gcode.sphere(argumentos, inside, traslado, ref salida);
                                break;
                            case "polyhedron":
                                throw new NotImplementedException();
                                break;
                        }
                    }
                    if (alerta)
                    {
                        alerta = false;
                        quitarModificador();

                    }
                    if (!union && difference)
                    {
                        difference = false;
                        inside = true;
                    }
                }
                #endregion
                #region transformaciones
                else if (defTransforma.IsMatch(linea))
                {
                    if (difference)
                        pendientes.AddLast(linea);
                    else
                    {
                        if (linea.Contains('{'))
                            linea = linea.Replace("{", "");
                        else
                            alerta = true;
                        string modificador = defTransforma.Match(linea).Value;
                        string argumentos = quitarCorchete(linea.Substring(linea.IndexOf('(') + 1, linea.LastIndexOf(')') - linea.IndexOf('(') - 1));
                        argumentos = reemplazarConstantes(argumentos);
                        argumentos = resolverOperaciones(argumentos);
                        switch (modificador)
                        {
                            case "translate":
                                translate.Push(new Coordenada(argumentos));
                                traslado = traslado + translate.Peek();
                                break;
                            case "rotate":
                                throw new NotImplementedException();
                                break;
                        }
                        modificadores.Push(modificador);
                        llaves.Push(false);
                    }
                }
                #endregion
                #region combinaciones
                else if (defCombina.IsMatch(linea))
                {
                    if (!linea.Contains('{'))
                        alerta = true;
                    string combinacion = linea.Substring(0, linea.IndexOf('('));
                    switch (combinacion)
                    {
                        case "difference":
                            difference = true;
                            break;
                        case "union":
                            union = true;
                            break;
                    }
                    modificadores.Push(combinacion);
                    llaves.Push(false);
                }
                #endregion
                #region llaves
                else if (defLlave.IsMatch(linea))
                {
                    if (defLlave.Match(linea).Value.Equals("{"))
                    {
                        alerta = false;
                        llaves.Pop();
                        llaves.Push(true);
                    }
                    else
                    {
                        quitarModificador();
                    }
                }
                #endregion
        }

        private static void expandFor2Args(LinkedListNode<string> entrada, string variable, string argumento)
        {
            string[] args = argumento.Split(':');
            expandFor3Args(entrada, variable, args[0] + ":1:" + args[1]);
        }

        private static void expandFor3Args(LinkedListNode<string> entrada, string variable, string argumento)
        {
            string[] args = resolverOperaciones(argumento).Split(':');
            string linea = "";
            string ocurrencia = "";
            double tope = Convert.ToDouble(args[2]);
            double delta = Convert.ToDouble(args[1]);
            double inicial = Convert.ToDouble(args[0]);
            Regex var1 = new Regex("\\W*" + variable + "\\W+");
            Regex var2 = new Regex("\\W+" + variable + "\\W*");
            if (inicial < tope)
            {
                for (double i = inicial; i <= tope; i = i + delta)
                {
                    foreach (string s in iteradores)
                    {
                        linea = s;
                        foreach (Match m in var1.Matches(linea))
                        {
                            ocurrencia = m.ToString().Replace(variable, i.ToString());
                            linea = linea.Replace(m.ToString(), ocurrencia);
                        }
                        foreach (Match m in var2.Matches(linea))
                        {
                            ocurrencia = m.ToString().Replace(variable, i.ToString());
                            linea = linea.Replace(m.ToString(), ocurrencia);
                        }
                        operaciones.AddBefore(entrada, linea);
                    }
                }
            }
            else
            {
                for (double i = inicial; i >= tope; i = Convert.ToDouble((i + delta).ToString(formato_num)))
                {
                    foreach (string s in iteradores)
                    {
                        linea = s;
                        foreach (Match m in var1.Matches(linea))
                        {
                            ocurrencia = m.ToString().Replace(variable, i.ToString(formato_num));
                            linea = linea.Replace(m.ToString(), ocurrencia);
                        }
                        foreach (Match m in var2.Matches(linea))
                        {
                            ocurrencia = m.ToString().Replace(variable, i.ToString(formato_num));
                            linea = linea.Replace(m.ToString(), ocurrencia);
                        }
                        operaciones.AddBefore(entrada, linea);
                    }
                }
            }
        }

        private static string resolverOperaciones(string argumentos)
        {
            string salida = "";
            string[] args;
            string separador ="";
            args = argumentos.Split(',');
            if (args.Length > 1)
                separador = ",";
            else
            {
                args = argumentos.Split(':');
                if (args.Length > 1)
                    separador = ":";
                else
                    separador = "";
            }
            string prefijo = "";
            try
            {
                prefijo = args[0].Substring(0, args[0].IndexOf('=') + 1);
                args[0] = args[0].Replace(prefijo, "");
            }
            catch (ArgumentOutOfRangeException ex){ }
            catch (ArgumentException ex) { }
            if(operacionesTrigonometricas.IsMatch(args[0]))
                args[0] = resolverTrig(args[0]);
            if(operacionesMath.IsMatch(args[0]))
                args[0] = resolverMath(args[0]);
            salida = prefijo + solve.solveString(args[0]).ToLower();
            for(int i=1;i<args.Length;i++)
            {
                prefijo = "";
                try
                {
                    prefijo = args[i].Substring(0, args[i].IndexOf('=') + 1);
                    args[i] = args[i].Replace(prefijo, "");
                }
                catch (ArgumentOutOfRangeException ex) { }
                catch (ArgumentException ex) { }
                if(operacionesTrigonometricas.IsMatch(args[i]))
                    args[i] = resolverTrig(args[i]);
                if (operacionesMath.IsMatch(args[i]))
                    args[i] = resolverMath(args[i]);
                salida += separador + prefijo + solve.solveString(args[i]).ToLower();
            }
            return salida;
        }

        private static string resolverTrig(string argumento)
        {
            //0.01745329252 = Pi/180
            string entrada = argumento;
            int indice = getBalancedString(entrada);
            string operacion = entrada.Substring(entrada.IndexOf(operacionesMath.Match(entrada).Value), indice);
            string argumentos = operacion.Substring(operacion.IndexOf("(") + 1, operacion.LastIndexOf(")") - operacion.IndexOf("(") - 1);
            switch (operacionesTrigonometricas.Match(entrada).Value.ToLower())
            {
                case "cos":
                    entrada = entrada.Replace(operacion, Math.Cos(Convert.ToDouble(resolverOperaciones("0.01745329252*" + argumentos))).ToString(formato_num));
                    break;

                case "sin":
                    entrada = entrada.Replace(operacion, Math.Sin(Convert.ToDouble(resolverOperaciones("0.01745329252*" + argumentos))).ToString(formato_num));
                    break;

                case "tan":
                    entrada = entrada.Replace(operacion, Math.Tan(Convert.ToDouble(resolverOperaciones("0.01745329252*" + argumentos))).ToString(formato_num));
                    break;

                case "acos":
                    entrada = entrada.Replace(operacion, (0.01745329252 * Math.Acos(Convert.ToDouble(resolverOperaciones(argumentos)))).ToString(formato_num));
                    break;

                case "asin":
                    entrada = entrada.Replace(operacion, (0.01745329252 * Math.Asin(Convert.ToDouble(resolverOperaciones(argumentos)))).ToString(formato_num));
                    break;

                case "atan":
                    entrada = entrada.Replace(operacion, (0.01745329252 * Math.Atan(Convert.ToDouble(resolverOperaciones(argumentos)))).ToString(formato_num));
                    break;

                case "atan2":
                    string[] args = argumentos.Split(',');
                    entrada = entrada.Replace(operacion, (0.01745329252 * Math.Atan2(Convert.ToDouble(resolverOperaciones(args[0])),Convert.ToDouble(resolverOperaciones(args[1])))).ToString(formato_num));
                    break;
            }
            return entrada;
        }

        private static string resolverMath(string argumento)
        {
            string entrada = argumento;
            int indice = getBalancedString(entrada);
            string operacion = entrada.Substring(entrada.IndexOf(operacionesMath.Match(entrada).Value), indice);
            string argumentos = operacion.Substring(operacion.IndexOf("(") + 1, operacion.LastIndexOf(")") - operacion.IndexOf("(") - 1);
            if (operacionesMath.IsMatch(argumentos))
                argumentos = resolverMath(argumentos);
            switch (operacionesMath.Match(entrada).Value.ToLower())
            {
                case "abs":
                    entrada = entrada.Replace(operacion, Math.Abs(Convert.ToDouble(argumentos)).ToString(formato_num));
                    break;
                case "ceil":
                    entrada = entrada.Replace(operacion, Math.Ceiling(Convert.ToDouble(argumentos)).ToString(formato_num));
                    break;
                case "concat":
                    throw new NotImplementedException();
                case "cross":
                    throw new NotImplementedException();
                case "exp":
                    entrada = entrada.Replace(operacion, Math.Exp(Convert.ToDouble(argumentos)).ToString(formato_num));
                    break;
                case "floor":
                    entrada = entrada.Replace(operacion, Math.Floor(Convert.ToDouble(argumentos)).ToString(formato_num));
                    break;
                case "ln":
                    entrada = entrada.Replace(operacion, Math.Log(Convert.ToDouble(argumentos),Math.E).ToString(formato_num));
                    break;
                case "len":
                    throw new NotImplementedException();
                case "let":
                    throw new NotImplementedException();
                case "log":
                    entrada = entrada.Replace(operacion, Math.Log10(Convert.ToDouble(argumentos)).ToString(formato_num));
                    break;
                case "lookup":
                    throw new NotImplementedException();
                case "max":
                    throw new NotImplementedException();
                case "min":
                    throw new NotImplementedException();
                case "norm":
                    throw new NotImplementedException();
                case "pow":
                    string[] args = argumentos.Split(',');
                    entrada = entrada.Replace(operacion, (Math.Pow(Convert.ToDouble(resolverOperaciones(args[0])),Convert.ToDouble(resolverOperaciones(args[1])))).ToString(formato_num));
                    break;
                case "rands":
                    throw new NotImplementedException();
                case "round":
                    entrada = entrada.Replace(operacion, Math.Round(Convert.ToDouble(argumentos)).ToString(formato_num));
                    break;
                case "sign":
                    if (Convert.ToDouble(argumentos) > 0)
                        entrada = "1.0";
                    else if (Convert.ToDouble(argumentos) == 0)
                        entrada = "0.0";
                    else
                        entrada = "-1.0";
                    break;
                case "sqrt":
                    entrada = entrada.Replace(operacion, Math.Sqrt(Convert.ToDouble(argumentos)).ToString(formato_num));
                    break;
            }
            return entrada;
        }

        static int getBalancedString(string arg)
        {
            int indice = 0;
            int parentesis = 0;
            while (arg[indice] != '(')
                indice++;
            do
            {
                if (arg[indice] == '(')
                    parentesis++;
                else if (arg[indice] == ')')
                    parentesis--;
                indice++;
            } while (parentesis != 0);
            return indice;
        }

        private static string quitarCorchete(string entrada)
        {
            try
            {
                entrada = entrada.Substring(entrada.IndexOf("[") + 1);
                entrada = entrada.Remove(entrada.LastIndexOf(']'));
            }
            catch (ArgumentOutOfRangeException ex){ }
            return entrada;
        }

        private static void quitarModificador()
        {
            do
            {
                string mod = modificadores.Pop();
                switch (mod)
                {
                    case "translate":
                        traslado = traslado - translate.Pop();
                        break;
                    case "difference":
                        inside = false;
                        ejecutarPendientes();
                        break;
                    case "union":
                        union = false;
                        if (difference)
                        {
                            inside = true;
                            difference = false;
                        }
                        break;
                    default:
                        break;
                }
                llaves.Pop();
            } while (llaves.Peek() == false);
        }

        private static void ejecutarPendientes()
        {
            string linea="";
            while (pendientes.Count > 0)
            {
                linea= pendientes.First();
                pendientes.RemoveFirst();
                analizarSintaxis(linea);
            }
        }

        private static void leerLineas()
        {
            procesarLineas(lector.ReadToEnd().Replace("\r","").Split('\n'));
            lector.DiscardBufferedData();
            lector.Close();
        }

        private static void procesarLineas(string[] lines)
        {
            Regex defComentarioLinea = new Regex("^\\/\\/.+");
            Regex defComentarioInicio = new Regex("^\\/\\*");
            Regex defComentarioFin = new Regex("\\*\\/");

            bool comentario = false;
            string temp;
            foreach (string s in lines)
            {
                temp = s;
                temp = Regex.Replace(temp, "\\s+", " ");
                temp = temp.Trim();
                #region detectar comentarios
                if (defComentarioLinea.IsMatch(temp))
                    temp = temp.Remove(temp.IndexOf(defComentarioLinea.Match(temp).ToString()));

                if (defComentarioInicio.IsMatch(temp))
                    comentario = true;

                if (defComentarioFin.IsMatch(temp))
                {
                    temp = temp.Remove(0, temp.IndexOf(defComentarioFin.Match(temp).ToString()) + 2);
                    comentario = false;
                }
                #endregion
                if (!comentario)
                {
                    if (temp.StartsWith("%") || temp.StartsWith("$fa") || temp.StartsWith("$fn") || temp.StartsWith("$fs"))
                        temp = ""; //Ignorar toda la línea

                    if (temp.Length > 0)
                    {
                        #region constantes
                        if (defConstante.IsMatch(temp))
                        {
                            //Reemplazar constantes dentro de constantes antes de agregar
                            string indice = temp.Substring(0, temp.IndexOf('=')).Trim();
                            string valor = temp.Substring(temp.IndexOf('=') + 1).Trim().Replace(";", "");
                            valor = reemplazarConstantes(valor);
                            constantes.AddLast(new Constante(indice, valor));
                        }
                        #endregion
                        else
                        {
                            if (temp.StartsWith("#"))
                                temp = temp.Substring(temp.IndexOf('#') + 1).Trim(); //Ignorar el modificador #
                            operaciones.AddLast(reemplazarConstantes(temp));
                        }
                    }
                }
            }
        }

        private static void codigoIntermedio()
        {
            Regex defFor = new Regex("^(?i)for\\s?\\(\\s?" + _asignacion);
            LinkedListNode<string> nodo = operaciones.First;
            while(nodo.Next != null)
            {
                string valor = nodo.Value;
                if (defFor.IsMatch(valor))
                {
                    string variable = Regex.Replace(defFor.Match(valor).ToString(), "\\s*", "");
                    variable = variable.Replace("for(", "");
                    variable = variable.Replace("=", "");
                    string argumento = reemplazarConstantes(quitarCorchete(defFor.Replace(valor, "")));
                    nodo = nodo.Previous;
                    expandirIteradores(nodo.Next, variable, argumento);
                    nodo = nodo.Next;
                    valor = nodo.Value;
                }

                string argumentos = valor;
                string argumentos_orig = argumentos;
                try
                {
                    argumentos = quitarCorchete(valor.Substring(valor.IndexOf('(') + 1, valor.LastIndexOf(')') - valor.IndexOf('(') - 1));
                    argumentos_orig = argumentos;
                }
                catch (ArgumentOutOfRangeException ex)
                { }
                argumentos = quitarCorchete(reemplazarConstantes(argumentos));
                if (defOperacion.IsMatch(argumentos))
                    argumentos = resolverOperaciones(argumentos);
                if (argumentos != argumentos_orig)
                    nodo.Value = valor.Replace(argumentos_orig, argumentos);
                nodo = nodo.Next;
            }
            nodo.Value = reemplazarConstantes(nodo.Value);
        }

        private static void buscarAlturaMayor()
        {
            double altura = 0;
            foreach (string s in operaciones)
            {
                altura = calcularAltura(s);
                if (altura > altura_maxima)
                    altura_maxima = altura;
            }
        }

        private static double calcularAltura(string s)
        {
            return 0;
        }
    }
}
